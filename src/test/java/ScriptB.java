import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class ScriptB {
    public static void main(String[] args) throws InterruptedException {

        WebDriver driver = getDriver();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

        driver.manage().window().maximize();

        WebElement emailField = driver.findElement(By.id("email"));
        emailField.sendKeys("webinar.test@gmail.com");

        WebElement passwordField = driver.findElement(By.id("passwd"));
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement submitLogin = driver.findElement(By.name("submitLogin"));
        submitLogin.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        //Orders
        WebElement orders = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminParentOrders"))));
        orders.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.className("page-title")).getText().equals("Заказы")) {
            System.out.println("User is not on the Заказы page.");
        }

        //Catalog
        WebElement catalog = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminCatalog"))));
        catalog.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.tagName("h2")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("товары")) {
            System.out.println("User is not on the товары page.");
        }

        //Customs
        WebElement customers = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[text()= 'Клиенты']/.."))));
        customers.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Управление клиентами")) {
            System.out.println("User is not on the Управление клиентами page.");
        }

        //Customer Service
        WebElement customerService = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminParentCustomerThreads"))));
        customerService.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Customer Service")) {
            System.out.println("User is not on the Customer Service page.");
        }

        //Statistics
        WebElement statistics = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminStats"))));
        statistics.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Статистика")) {
            System.out.println("User is not on the Статистика page.");
        }

        //Modules
        WebElement modules = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminParentModulesSf"))));
        modules.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.tagName("h2")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Выбор модуля")) {
            System.out.println("User is not on the Выбор модуля page.");
        }

        //Design
        WebElement design = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[text()='Design']/.."))));
        design.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Шаблоны > Шаблон")) {
            System.out.println("User is not on the Шаблоны > Шаблон  page.");
        }

        //Доставка
        WebElement delivery = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminParentShipping"))));
        delivery.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Перевозчики")) {
            System.out.println("User is not on the Шаблоны > Шаблон  page.");
        }

        //Payment Methods
        WebElement paymentMethods = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminParentPayment"))));
        paymentMethods.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Payment Methods")) {
            System.out.println("User is not on the Payment Methods page.");
        }

        //International
        WebElement international = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminInternational"))));
        international.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Локализация")) {
            System.out.println("User is not on the Локализация page.");
        }

        //Shop Parameters
        WebElement shopParam = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-ShopParameters"))));
        shopParam.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("General")) {
            System.out.println("User is not on the General page.");
        }

        //Конфигурация
        WebElement config = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("subtab-AdminAdvancedParameters"))));
        config.click();

        System.out.println("Headers name is " + wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("page-title")))).getText());
        driver.navigate().refresh();

        if (!driver.findElement(By.tagName("h2")).getText().equals("Information")) {
            System.out.println("User is not on the Information page.");
        }

        driver.quit();
    }

    private static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//resources//chromedriver.exe");
        return new ChromeDriver();
    }
}
